package com.gaussian.iup;

import java.io.IOException;

import com.gaussian.iup.helper.InputFileHelper;
import com.gaussian.iup.helper.OutputFileHelper;
import com.gaussian.iup.helper.RandomNumberHelper;
import com.gaussian.iup.model.GaussianModel;

public class Main {

	public static void main(String[] args) throws NumberFormatException,
			IOException {
		// Using Input
		// GaussianModel model = InputFileHelper.read("D:/input.txt");
		// Create Random
		GaussianModel model = new GaussianModel(
				RandomNumberHelper.generateMatrix(3),
				RandomNumberHelper.generateResult(3));
		OutputFileHelper.writeMatrixAndResultMake("D:/random.txt",
				model.getMatrix(), model.getResult());
		Double[] solution = GaussianElimination.GaussianPartialPivotting(
				model.getMatrix(), model.getResult());
		OutputFileHelper.writeSolutionAppend("D:/random.txt", solution);
	}
}