package com.gaussian.iup;

public class GaussianElimination {
	/**
	 * GaussianElimination.java
	 * Purpose: Naive Gaussian Elimination method
	 * @author Haydar
	 */
	private static final double EPSILON = 1e-10;

	/**
	 * Checking whether the matrix is singular or not
	 * @param matrix
	 * @param p
	 * @return boolean
	 */
	private static Boolean isSingular(Double[][] matrix, int p) {
		if (Math.abs(matrix[p][p]) <= EPSILON) {
			throw new RuntimeException("Matrix is singular or nearly singular");
		} else {
			return true;
		}
	}

	/**
	 * Doing back substitution
	 * @param matrix
	 * @param result
	 * @return solution of the problem
	 */
	private static Double[] backSubstitution(Double[][] matrix, Double[] result) {
		Double[] solution = new Double[result.length];
		for (int i = result.length - 1; i >= 0; i--) {
			Double sum = 0.0;
			for (int j = i + 1; j < result.length; j++) {
				sum += matrix[i][j] * solution[j];
			}
			solution[i] = (result[i] - sum) / matrix[i][i];
		}
		return solution;
	}

	/**
	 * Swapping rows
	 * @param matrix
	 * @param result
	 * @param row a
	 * @param row b
	 */
	private static void swapRows(Double[][] matrix, Double[] result, int a,
			int b) {
		// Swapping matrix rows
		Double[] temp = matrix[a];
		matrix[a] = matrix[b];
		matrix[b] = temp;

		// Swapping results rows
		Double t = result[a];
		result[a] = result[b];
		result[b] = t;
	}

	/**
	 * Finding pivot
	 * @param matrix
	 * @param result
	 * @param p
	 * @return pivot's row
	 */
	private static int findPivot(Double[][] matrix, Double[] result, int p) {
		int max = p;
		for (int i = p + 1; i < result.length; i++) {
			if (Math.abs(matrix[i][p]) > Math.abs(matrix[max][p])) {
				max = i;
			}
		}
		return max;
	}

	public static Double[] GaussianPartialPivotting(Double[][] matrix,
			Double[] result) {
		for (int p = 0; p < result.length; p++) {
			int max = findPivot(matrix, result, p);
			swapRows(matrix, result, p, max);
			isSingular(matrix, p);

			for (int i = p + 1; i < result.length; i++) {
				Double alpha = matrix[i][p] / matrix[p][p];
				result[i] -= alpha * result[p];
				for (int j = p; j < result.length; j++) {
					matrix[i][j] -= alpha * matrix[p][j];
				}
			}
		}
		return backSubstitution(matrix, result);
	}
}