package com.gaussian.iup.model;

/**
 * GaussianModel.java Purpose: Model of matrix, the result, and combination of
 * both of them
 * 
 * @author Haydar
 */
public class GaussianModel {

	private Double[][] matrix;
	private Double[] result;

	public Double[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(Double[][] matrix) {
		this.matrix = matrix;
	}

	public Double[] getResult() {
		return result;
	}

	public void setResult(Double[] result) {
		this.result = result;
	}

	public GaussianModel(Double[][] matrix, Double[] result) {
		super();
		this.matrix = matrix;
		this.result = result;
	}
}
