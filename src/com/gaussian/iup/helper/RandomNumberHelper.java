package com.gaussian.iup.helper;

import java.io.IOException;
import java.util.Random;

public class RandomNumberHelper {

	private static Double[][] matrix;
	private static Double[] result;

	public static Double[][] generateMatrix(int size) throws IOException {
		matrix = new Double[size][size];
		matrix = generateArrayMatrix(size);
		return matrix;
	}

	public static Double[] generateResult(int size) throws IOException {
		result = new Double[size];
		result = generateArrayResult(size);
		return result;
	}

	private static Double generateDouble() {
		Random generator = new Random();
		return generator.nextDouble() * 2000 - 1000;
	}

	private static Double[][] generateArrayMatrix(int size) {
		Double[][] array = new Double[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				array[i][j] = generateDouble();
			}
		}
		return array;
	}

	private static Double[] generateArrayResult(int size) {
		Double[] array = new Double[size];
		for (int i = 0; i < size; i++) {
			array[i] = generateDouble();
		}
		return array;
	}
}
