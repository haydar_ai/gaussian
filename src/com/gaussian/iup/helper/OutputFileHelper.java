package com.gaussian.iup.helper;

import java.io.FileWriter;
import java.io.IOException;

public class OutputFileHelper {

	public static void writeMatrixAndResultMake(String fileName,
			Double[][] matrix, Double[] result) throws IOException {
		FileWriter fw = new FileWriter(fileName, false);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				fw.write(String.valueOf(matrix[i][j]));
				fw.write("\t");
			}
			fw.write("X" + (i + 1));
			fw.write("\t");
			fw.write(String.valueOf(result[i]));
			fw.write(System.lineSeparator());
		}
		fw.close();
	}

	public static void writeMatrixAndResultAppend(String fileName,
			Double[][] matrix, Double[] result) throws IOException {
		FileWriter fw = new FileWriter(fileName, true);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				fw.write(String.valueOf(matrix[i][j]));
				fw.write("\t");
			}
			fw.write("X" + (i + 1));
			fw.write("\t");
			fw.write(String.valueOf(result[i]));
			fw.write(System.lineSeparator());
		}
		fw.close();
	}

	public static void writeSolutionMake(String fileName, Double[] solution)
			throws IOException {
		FileWriter fw = new FileWriter(fileName, false);
		for (int i = 0; i < solution.length; i++) {
			fw.write("X" + (i + 1) + " = " + String.valueOf(solution[i]));
			fw.write(System.lineSeparator());
		}
		fw.close();
	}

	public static void writeSolutionAppend(String fileName, Double[] solution)
			throws IOException {
		FileWriter fw = new FileWriter(fileName, true);
		for (int i = 0; i < solution.length; i++) {
			fw.write("X" + (i + 1) + " = " + String.valueOf(solution[i]));
			fw.write(System.lineSeparator());
		}
		fw.close();
	}
}
