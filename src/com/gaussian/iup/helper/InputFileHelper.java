package com.gaussian.iup.helper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.gaussian.iup.model.GaussianModel;

/**
 * InputFileHelper.java Purpose: Read input file
 * 
 * @author Haydar Ali Ismail
 */
public class InputFileHelper {

	private static BufferedReader br;

	/**
	 * Read input file of matrices
	 * 
	 * @param fileName
	 * @return GaussianModel
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public static GaussianModel read(String fileName)
			throws NumberFormatException, IOException {
		br = new BufferedReader(new FileReader(fileName));
		String[] sResult = br.readLine().split("\\s+");
		Double[] dResult = new Double[sResult.length];
		for (int i = 0; i < sResult.length; i++) {
			dResult[i] = Double.valueOf(sResult[i]);
		}
		Double[][] matrix = new Double[dResult.length][dResult.length];
		for (int i = 0; i < dResult.length; i++) {
			String[] row = br.readLine().split("\\s+");
			for (int j = 0; j < dResult.length; j++) {
				matrix[i][j] = Double.valueOf(row[j]);
			}
		}
		return new GaussianModel(matrix, dResult);
	}
}
